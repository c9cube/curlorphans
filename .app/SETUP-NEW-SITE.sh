#!/bin/bash

read -p "Dali si kreirao prazan bitbucket repozitorij sa imenom aplikacije? Ako ne, učini to prije nego pritisneš enter..."

source ~/workspace/.app/site/_appdata.io;
cd ~/workspace;
rhc account logout;
rhc setup --server openshift.redhat.com --clean -l $login -p $password --create-token;

echo "**************************************************"
echo ""
echo "KREIRANJE NOVE APLIKACIJE TRAJE NEKOLIKO MINUTA..."
echo ""
echo "**************************************************"

rhc app create $appname php-5.4 -s;
rhc cartridge add mysql-5.5 -a $appname;

suffix=".io";
sitefile=$appname$suffix;
sitefilepath=~/workspace/.app/site/$sitefile;

#ako postoji izbriši ga i napiši ponovno
truncate -s0 $sitefilepath;
rhc app-show -v $appname >> $sitefilepath;
printf "\n\n-----------------------------------------------------\n" >> $sitefilepath;
printf "\nHost: " >> $sitefilepath;
rhc ssh $appname 'echo $OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT' >> $sitefilepath;
printf "\n-----------------------------------------------------" >> $sitefilepath;

#PARSING APP NAMES
#parse appname
#ma uopče ne treba mjenjati mapu...
#cd ~/workspace/.app/site;

app=$appname
app+=".io"
tmpsite=$(head -1 $app)
siteid=$(grep -oP '(?<=\()[^\)]+' <<< "$tmpsite")
siteid2=${siteid:6:24}
sitename=$(grep -oP '//\K.*?(?=/)' <<< "$tmpsite")

printf "\n" >> ~/workspace/.app/site/_appdata.io

#ok ovo je giturl
giturl="giturl=\"ssh://"
giturl+=$siteid2
giturl+="@"
giturl+=$sitename
giturl+="/~/git/"
giturl+=$appname
giturl+=".git/\"" 

printf $giturl >> ~/workspace/.app/site/_appdata.io
printf "\n" >> ~/workspace/.app/site/_appdata.io

#a ssh adresa je ovo
ssh="ssh=\""
ssh+=$siteid2
ssh+="@"
ssh+=$sitename 
ssh+="\""

printf $ssh >> ~/workspace/.app/site/_appdata.io
printf "\n" >> ~/workspace/.app/site/_appdata.io

user="user=\""
user+=$siteid2
user+="\""

printf $user >> ~/workspace/.app/site/_appdata.io
printf "\n" >> ~/workspace/.app/site/_appdata.io

currentsite="site=\""
currentsite+=$sitename
currentsite+="\""

printf $currentsite >> ~/workspace/.app/site/_appdata.io
#END PARSING APP NAMES

#cijelo vrijeme smo u ~/workspace
#cd ~/workspace;
git init;
git remote add -t \* -f origin $giturl;
git checkout master;
git remote add quickstart -m master $gitsource;

echo "********************"
echo ""
echo "UPUTE ZA NANO EDITOR"
echo ""
echo "********************"

read -p "ZAPAMTI, KADA SE POJAVI NANO EDITOR PRITISNI CTRL+X, A SADA PRITISNI ENTER: "

#OVDJE TREBA NADODATI IZBOR VERZIJE QUICKSTARTA
#U STVARI TO BI TREBALO NAPRAVITI NA POČETKU
git pull -s recursive -X theirs quickstart master;
chmod +x ~/workspace/.openshift/action_hooks/deploy;
git add .;
git commit -am "My first deploy";
source ~/workspace/.app/resources/mysql.sh;
git push origin master;

#sačuvamo podatke iz appdata, da se ne zaborave...
F1=~/workspace/.app/site/_appdata.io
F2=$sitefilepath
printf "\n\n" >> "$F2"
cat "$F1" >> "$F2"

cd ~/workspace;
rhc app-force-stop $appname;

echo "**************************************************"
echo ""
echo "OVO ZNA ISTO MALO POTRAJATI..."
echo ""
echo "**************************************************"

rhc app-tidy $appname;
rhc app-start $appname;

#možda mogu ovdje odmah naddati hot deploy...
cd ~/workspace;
touch ~/workspace/.openshift/markers/hot_deploy;
chmod a=r+w+x ~/workspace/.openshift/markers/hot_deploy;

git add -A;
git commit -am "Add hot_deploy";
git push origin;

#remove origin
#remove quickstart (TO JE BITBUCKET QUICKSTART)
#add bitbucket
git remote rm quickstart;
#ok, ovo je možda malo pretjerano, ali ipak
#da se ne dovedem opet u napast :)
git remote rm origin;
gitname=$appname;
gitname+=".git";
git remote add bitbucket -m master git@bitbucket.org:c9cube/$gitname;
#curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/1.0/repositories/ --data name=$appname
git push bitbucket master;

echo "**************************************************"
echo ""
echo "GIT REMOTE:"
echo ""
echo "**************************************************"

git remote -v;

echo "**************************************************"
echo ""
echo " END GIT REMOTE;"
echo ""
echo "**************************************************"

cd ~/workspace;
rhc app-configure $appname --keep-deployments 1;

#ovdje mogu nadodati još skriptu za domene, ako ima domena, tako da se to odmah sredi
#a ako nema domene, to se preskoči

echo "*******************************************************"
echo ""
echo "TO BI BILO TO, KONFIGURIRAJ APLIKACIJU NA OPENSHIFTU..."
echo ""
echo "*******************************************************"
