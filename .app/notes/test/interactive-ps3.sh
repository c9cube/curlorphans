#!/bin/bash

RED="\033[0;31m"
YELLOW="\033[1;33m"
GREEN="\033[1;32m"
GREEN_BOLD="\033[01;32m"
BLUE="\033[1;34m"
BLUE_BOLD="\033[01;34m"
LIGHT_RED="\033[1;31m"
LIGHT_GREEN="\033[1;32m"
CYAN="\033[0;36m"
LIGHT_CYAN="\033[1;36m"
WHITE="\033[1;37m"
LIGHT_GRAY="\033[0;37m"
COLOR_NONE="\e[0m"

bitbucket_site_url=""
branch=""
openshift_site_name=""
openshift_site_login=""
openshift_site_password=""
openshift_site_domain=""

PS3="Select COMMAND (1-4): "
select i in "GET REPOSITORY" "GET BRANCHES" "KRNEKI" exit
do
  case $i in
    "GET REPOSITORY")
        echo
        echo -e "${BLUE}REPOSITORIES:"
            source /home/ubuntu/workspace/curlorphans/.app/resources/GET_REPOSITORIES.sh
            echo -e "${GREEN}REPOSITORIES:"
            cat /home/ubuntu/workspace/curlorphans/.app/resources/repositories.txt
        echo -e "${COLOR_NONE}\n"
            read -p "PLEASE ENTER REPOSITORY: " site
        echo -e "${BLUE}BRANCHES:"
            source /home/ubuntu/workspace/curlorphans/.app/resources/GET_BRANCHES.sh
            echo -e "${GREEN}BRANCHES:"
            cat /home/ubuntu/workspace/curlorphans/.app/resources/branches.txt
        echo -e "${COLOR_NONE}\n"
            read -p "PLEASE ENTER BRANCH: " site
        echo ;;
    "GET BRANCHES")
        echo
        echo -e "${BLUE}BRANCHES:"
        echo -e "${GREEN}\n"
            read -p "Please enter site: " site
        echo -e "${COLOR_NONE}"
            curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/1.0/repositories/c9cube/$site/branches/ \
            | JSON.sh -b \
            | awk '/"branch"/' \
            | sed 's/\[[^][]*\]//g' \
            | sed 's/\"//g' \
            | sed 's/[[:blank:]]//g'
        echo
        echo -e "${BLUE}BRANCHES END"
        echo -e "${COLOR_NONE}";;
    "KRNEKI") 
        echo $PWD
        echo "KRNEKI END";;
    exit) 
        exit;;
  esac
done