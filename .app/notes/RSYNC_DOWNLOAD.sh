
#DOWNLOAD
rsync -rmc ~/workspace/..mounts/hello/wp-content/            ~/workspace/hello/wp-content 
rsync -rmc ~/workspace/..mounts/hello/wp-content/plugins/    ~/workspace/hello/wp-content/plugins
rsync -rmc ~/workspace/..mounts/hello/wp-content/themes/     ~/workspace/hello/wp-content/themes

#FAST RSYNC OVER SSH
#download
rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x" \
56de947a0c1e665d8300009c@hello-istria.rhcloud.com:/var/lib/openshift/56de947a0c1e665d8300009c/app-root/runtime/repo/ \
~/workspace/haloistrassh

#upload
rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x" \
~/workspace/haloistrassh/ \
56de947a0c1e665d8300009c@hello-istria.rhcloud.com:/var/lib/openshift/56de947a0c1e665d8300009c/app-root/runtime/repo


Ok, zanima me, da li to radi i sa dodavanjem i delete-anjem file-ova

Tasks: 
    - download all changes / sve promjene, koje su napravljene na serveru idu pod istu verziju, verzije se može mjenjati samo na c9 /
    - + wp-sync-db PULL
    - razvoj na c9, kreiranje nove verzije i push nove verzije u bitbucket
    - i onda samo rsync ssh na server, i to je to!
    - + wp-sync-db PUSH
    
    OK, TREBA SAMO INSTALACIJSKU SKRIPTU PRILAGODITI NA ROOT /+ONDA MI NE TREBA NIKAKVE APP_C9 KOMPLICAKIJE, MOŽE OSTATI C9/
    + NADODATI OVE VARIABLE: SITEURL, USER

#DOWNLOAD
rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x" \
56de947a0c1e665d8300009c@hello-istria.rhcloud.com:/var/lib/openshift/56de947a0c1e665d8300009c/app-root/runtime/repo/ .

#UPLOAD
rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x" \
. 56de947a0c1e665d8300009c@hello-istria.rhcloud.com:/var/lib/openshift/56de947a0c1e665d8300009c/app-root/runtime/repo

exclude: --exclude ".*/"

#generično:
source ~/workspace/.app/site/_appdata.io;
#DOWNLOAD
rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x" $user@$site:/var/lib/openshift/$user/app-root/runtime/repo/ .
#UPLOAD
rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x" . $user@$site:/var/lib/openshift/$user/app-root/runtime/repo
