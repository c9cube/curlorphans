#!/bin/bash

source ~/workspace/.app/site/_appdata.io;
cd ~/workspace/$appname;
  repo_name=$1

  dir_name=`basename $(pwd)`

  if [ "$repo_name" = "" ]; then
    echo "Repo name (hit enter to use '$dir_name')?"
    read repo_name
  fi

  if [ "$repo_name" = "" ]; then
    repo_name=$dir_name
  fi

  username=$github_user
     echo "Using github user: $username"
  if [ "$username" = "" ]; then
    echo "Could not find username, run 'git config --global github.user <username>' ..."
    echo "...Or write github_user to _appdata.io"
    invalid_credentials=1
  fi

  token=$github_token
  if [ "$token" = "" ]; then
    echo "Could not find token, run 'git config --global github.token <token>' ..."
    echo "...Or write github_token to _appdata.io"
    echo "If you don't have token, create one here: 'https://github.com/settings/tokens/new' !"
    invalid_credentials=1
  fi

  if [ "$invalid_credentials" == "1" ]; then
    return 1
  fi

  echo -n "Creating Github repository '$repo_name' ..."
  curl -u "$username:$token" https://api.github.com/user/repos -d '{"name":"'$repo_name'"}' > /dev/null 2>&1
  echo " done."

  echo -n "Pushing local code to github ..."
  git remote add github git@github.com:$username/$repo_name.git > /dev/null 2>&1
  git push -u github master > /dev/null 2>&1
  echo " done."