#!/bin/bash

cd ~/workspace/.app;
sudo chmod +x ./*.sh;
cd ~/workspace/.app/resources;
sudo chmod +x ./*.sh;

sudo apt-get install ruby-full;
sudo apt-get install rubygems;
sudo apt-get install git-core;
sudo gem install rhc;

cd ~/workspace;
touch .gitignore;
printf "!.app\n!.openshift\n.c9" >> ~/workspace/.gitignore
