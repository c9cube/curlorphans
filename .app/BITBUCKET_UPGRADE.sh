#!/bin/bash
read -p "Please enter NEW SITE VERSION: " newversion

source ~/workspace/.app/site/_appdata.io
cd ~/workspace

git checkout -b $newversion
git  add -A
v="New version:"
v+=$newversion
git commit -m $v

truncate -s0 ~/workspace/.app/site/_version.io;
v="version=\""
v+=$newversion
v+="\""
printf $v >> ~/workspace/.app/site/_version.io
