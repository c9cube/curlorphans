#!/bin/bash
source ~/workspace/.app/site/_appdata.io;
cd ~/workspace;
rhc app-force-stop $appname;
rhc app-tidy $appname;
rhc app-start $appname;
