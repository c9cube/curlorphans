#!/bin/bash
read -p "Please enter site version to pull: " version

source ~/workspace/.app/site/_appdata.io
cd ~/workspace

rm -rf * !(.app|.c9|.|..)
git init
a=$appname
a+=".git"
git remote add bitbucket git@bitbucket.org:c9cube/$a
git pull bitbucket $version
git checkout $version

truncate -s0 ~/workspace/.app/site/_version.io;
v="version=\""
v+=$version
v+="\""
printf $v >> ~/workspace/.app/site/_version.io
