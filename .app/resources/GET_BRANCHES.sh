#!/bin/bash
curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/1.0/repositories/c9cube/curlorphans/branches/ \
| JSON.sh -b \
| awk '/"branch"/' \
| sed 's/\[[^][]*\]//g' \
| sed 's/\"//g' \
| sed 's/[[:blank:]]//g' > branches.txt
 