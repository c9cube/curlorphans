#!/bin/bash
source ~/workspace/.app/site/_appdata.io;

dbname=$appname
mysql-ctl cli << MYSQL
DROP DATABASE $dbname;
CREATE DATABASE $dbname;
MYSQL
