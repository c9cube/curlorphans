#!/bin/bash
source ~/workspace/.app/site/_appdata.io;

#production dump
ssh -t $ssh << 'ENDSSH'
mysqldump -h $OPENSHIFT_MYSQL_DB_HOST -P ${OPENSHIFT_MYSQL_DB_PORT:-3306} \
-u ${OPENSHIFT_MYSQL_DB_USERNAME:-'admin'} --password="$OPENSHIFT_MYSQL_DB_PASSWORD" \
--databases $OPENSHIFT_APP_NAME > /var/lib/openshift/56de947a0c1e665d8300009c/app-root/runtime/repo/wp-content/mysqldump/production.sql
ENDSSH

cp ~/mounts/haloistra/wp-content/mysqldump/production.sql ~/workspace/.app/notes/test

#staging dump
dbname=$appname
dbname+="_c9"
mysqldump -h $IP -P 3306 -u $C9_USER --password="" --databases $dbname > ~/workspace/.app/notes/test/staging.sql
mysql -h $IP -P 3306 -u $C9_USER --password="" --databases $dbname < ~/workspace/.app/notes/test/production.1.sql


#ovdje još treba vidjeti kako je sa domenama...+ treba prije provjeriti da su jezici instalirani
#ma u stvari to ne bi smio biti problem, obzirom da je sve to u bazi...
#ima primjera kada se site url pojavljuje bez http:// 
#full staging url
site=$appname
project=$C9_PROJECT
staging_url="https://"
staging_url+=$project
staging_url+="-infocube.c9users.io/"
staging_url+=$site

#mysqldump staging -> production
#mysqldump production -> staging

production_dir="\/var\/lib\/openshift\/56de947a0c1e665d8300009c\/app-root\/runtime\/repo"
production_url=$site

staging_dir="~\/workspace\/.app\/notes\/test"
staging_url=

sed 's/$production_url/$staging_url/g' ~/workspace/.app/notes/test/production.sql > ~/workspace/.app/notes/test/staging.sql


mysql -u username -p -h localhost DATA-BASE-NAME < data.sql
