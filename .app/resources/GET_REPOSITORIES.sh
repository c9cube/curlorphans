#!/bin/bash

page=1
curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/2.0/repositories/c9cube?page=$page > jsondata.json;
for true in grep 'next' jsondata.json
do
    curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/2.0/repositories/c9cube?page=$page \
    | JSON.sh | awk '/"links","clone",1,"href"/' \
    | sed 's/\[[^][]*\]//g' \
    | sed 's/\"//g' \
    | sed 's/[[:blank:]]//g' >> repositories.txt;
    let page=$page+1
    echo "Page: "$page
    curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/2.0/repositories/c9cube?page=$page > jsondata.json;
done